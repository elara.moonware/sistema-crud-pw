
  <head>
    <meta name="description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@pratikborsadiya">
    <meta property="twitter:creator" content="@pratikborsadiya">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Vali Admin">
    <meta property="og:title" content="Vali - Free Bootstrap 4 admin theme">
    <meta property="og:url" content="http://pratikborsadiya.in/blog/vali-admin">
    <meta property="og:image" content="http://pratikborsadiya.in/blog/vali-admin/hero-social.png">
    <meta property="og:description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <title>Sistema MVC</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Essential javascripts for application to work-->
    <script src="assets/js/jquery-3.4.1.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="index.html">Sistema</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <!-- User Menu-->
        <a href="#" data-toggle="modal" data-target="#registrarModal" class="app-nav__item">
          <i class="fa fa-lg fa-user"> Registrar</i>
        </a>
        <a href="#" data-toggle="modal" data-target="#ingresarModal" class="app-nav__item">
          <i class="fa fa-lg fa-sign-in"> Ingresar</i>
        </a>
      </ul>
    </header>

<!-- Modal Registro -->
<div class="modal fade" id="registrarModal" tabindex="-1" role="dialog" aria-labelledby="registrarModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="registrarModal">Registra tus datos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form >
        <div class="form-group">
          <label for="nombre">Nombre Completo</label>
          <input type="text" class="form-control" id="nombre" placeholder="Introduzca su nombre">
        </div>
        <div class="form-group">
          <label for="usuario">Usuario</label>
          <input type="text" class="form-control" id="usuario" placeholder="Introduzca su usuario">
        </div>
        <div class="form-group">
          <label for="password1">Contraseña</label>
          <input type="password" class="form-control" id="password1" placeholder="Contraseña">
        </div>
        <div class="form-group">
          <label for="passwordConfirm">Confirme su Contraseña</label>
          <input type="password" class="form-control" id="password2" placeholder="Confirme su Contraseña">
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" id="btnregistrar" class="btn btn-success">Registrar</button>
      </div>
        </form>
      </div>
      
    </div>
  </div>
</div>

<!--Modal Ingreso-->
<div class="modal fade" id="ingresarModal" tabindex="-1" role="dialog" aria-labelledby="ingresarModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ingresarModal">Ingresar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="">
        <div class="form-group">
          <label for="usuario">Usuario</label>
          <input type="text" class="form-control" id="usuarioIngresar" placeholder="Introduzca su usuario">
        </div>
        <div class="form-group">
          <label for="password1">Contraseña</label>
          <input type="password" class="form-control" id="passwordIngresar" placeholder="Contraseña">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" id="btningresar" class="btn btn-success">Ingresar</button>
      </div>
    </div>
  </div>
</div>
<!--Modal Validacion-->
<div class="modal" tabindex="-1" id="mensajeModal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" bg-warning>
        <h5 class="modal-title" id="mensajeModalLabel">Mensaje</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id = "mensaje"></p>
        <input type="hidden" name="idmodal" id="idmodal">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(function (){
  $('#btnregistrar').on('click', function(){
    var fnombre = $('#nombre').val();
    var fusuario = $('#usuario').val();
    var fpassword1 = $('#password1').val();
    var fpassword2 = $('#password2').val();

    var error=false;

    if(fnombre === ""){
      $('#mensaje').text("El nombre no puede ser vacio");
      $('#mensajeModal').modal("show");
      error = true;
    }
    if(fusuario === ""){
      $('#mensaje').text("El usuario no puede ser vacio");
      $('#mensajeModal').modal("show");
      error = true;
    }
    if(fpassword1 === ""){
      $('#mensaje').text("El password no puede ser vacio");
      $('#mensajeModal').modal("show");
      error = true;
    }
    if(fpassword1 != fpassword2){
      //alert("Los passwords no coinciden");
      $('#mensaje').text("Contraseña no coinciden");
      $('#mensajeModal').modal("show");
      error = true;
    }if(error===false){
      $.ajax({
        url: '?c=usuarios&a=Registrar',
        method: 'POST',
        data: {
          nombre: fnombre,
          login: fusuario,
          password: fpassword1
        },
        success: (response)=> {
           if(response.registro == "ok"){
              location.href='?c=producto';
           }
           if(response.registro == 'existe'){
               $('#mensaje').text('Error: usuario ya esta registrado');
           }else{
               $('#mensaje').text('Error: al registrar el usuario');
            }
            $('#mensajeModal').modal('show');
          }
        }); //ajax
       }//else  
  });
});

</script>

<script type="text/javascript">
  $(function (){
    $('#btningresar').on('click', function(){
    var fusuario = $("#usuarioIngresar").val();
    var fpassword = $("#passwordIngresar").val();
    $.ajax({
      url : "?c=usuarios&a=Ingresar",
      method : "POST",
      data : {
        login : fusuario,
        password : fpassword
      },
      success: (response)=> {
        if (response.login == "ok"){
          location.href = "?c=producto";
        }
        if (response.login == "error"){
          $("#mensaje").text("Nombre de usuario o clave incorrecta");
          $("#mensajeModal").modal("show");
        }else if(response.logn == udefined){
          $("#mensaje").text("Error: al iniciar sesion");
          $("#mensajeModal").modal("show");
        }
      }
    });
  });
  });
</script>