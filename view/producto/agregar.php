
<?php
        if(isset($_SESSION['login'])){
          $login = $_SESSION['login'];
          $nombre = $_SESSION['nombre'];
          $id = $_SESSION['id'];
        }else{
          header("location:?c=usuarios");
        }
      ?>
<main class="app-content">

<div class="col-md-6">
          <div class="tile">
            <h3 class="tile-title"><?=$titulo?></h3>
            <div class="tile-body">
              <form class="form-horizontal" method="POST" action="?c=producto&a=Guardar">
                <div class="form-group row">
                  <label class="control-label col-md-3">Nombre</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" name="nombreProd" value="<?=$p->getNombre()?>" placeholder="Ingrese el nombre del producto">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Marca</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" name="marcaProd" value="<?=$p->getMarca()?>" placeholder="Marca">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Costo</label>
                  <div class="col-md-8">
                    <input class="form-control" type="number" name="costoProd" value="<?=$p->getCosto()?>" placeholder="Costo del producto" min="0">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Precio</label>
                  <div class="col-md-8">
                    <input class="form-control" type="number" name="precioProd" value="<?=$p->getPrecio()?>" placeholder="Precio del producto">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Cantidad</label>
                  <div class="col-md-8">
                    <input class="form-control" type="number" name="cantidadProd" value="<?=$p->getCantidad()?>" placeholder="Ingrese la cantidad del producto">
                  </div>
                </div>
              <input type="hidden" class="form-control" name="idProducto" value="<?=$p->getId()?>" >
            </div>
            <div class="tile-footer">
              <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle">
                  </i><?=$titulo?></button>
                </div>
              </div>
            </div>
            </form>
          </div>
</div>

</main>