
  <head>
    <meta name="description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@pratikborsadiya">
    <meta property="twitter:creator" content="@pratikborsadiya">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Vali Admin">
    <meta property="og:title" content="Vali - Free Bootstrap 4 admin theme">
    <meta property="og:url" content="http://pratikborsadiya.in/blog/vali-admin">
    <meta property="og:image" content="http://pratikborsadiya.in/blog/vali-admin/hero-social.png">
    <meta property="og:description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <title>Sistema MVC</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Essential javascripts for application to work-->
<script src="assets/js/jquery-3.4.1.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="assets/js/plugins/pace.min.js"></script>
    <!-- Page specific javascripts-->
  </head>
  <body class="app sidebar-mini rtl">
      <?php
        if(isset($_SESSION['login'])){
          $login = $_SESSION['login'];
          $nombre = $_SESSION['nombre'];
          $id = $_SESSION['id'];
        }else{
          header("location:?c=usuarios");
        }
      ?>
      
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="index.html">Sistema</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <!-- User Menu-->
        <a href="?c=producto&a=FormAgregar" class="btn btn-success btn-flat">
          <i class="fa fa-lg fa-plus"></i>
        </a>
        <a href="?c=producto&a=Salir" class="btn btn-danger btn-flat">
          <i class="fa fa-lg">Cerrar Sesion</i>
        </a>
      </ul>
    </header>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image">
        <div>
          <p class="app-sidebar__user-name">Bienvenido <?=$nombre?></p>
          <p class="app-sidebar__user-designation"><?=$login?></p>
        </div>
      </div>
      <ul class="app-menu">
       
        <li>
        <a class="app-menu__item" href="?c=producto"><i class="app-menu__icon fa fa-th-list">
        </i><span class="app-menu__label">Productos</span>
        </a>
        </li>
        <li>
        <a class="app-menu__item" href="?c=producto&a=FormAgregar"><i class="app-menu__icon fa fa-plus">
        </i><span class="app-menu__label">Agregar</span>
        </a>
        </li>

      </ul>
    </aside>