<?php
require_once "model/usuarios.php";
class UsuariosControlador
{
    private $usuarios;

    public function __CONSTRUCT(){
        $this->usuarios = new Usuarios();
    }

    public function Registrar(){
        $u = new Usuarios();
        $log = $_POST['login'];
        $u->setLogin($log);
        $u->setPassword($_POST['password']);
        $u->setNombre($_POST['nombre']);

        //validamos que no exista el usuario
        $existe = $this->usuarios->ExisteUsuario($u->getLogin());

        if($existe){
            $resp = "existe";
        }else{
            $resp ='ok';
            $this->usuarios->Insertar($u);

            $_SESSION['login'] = $_POST['login'];
            $_SESSION['nombre'] = $_POST['nombre'];
            
            $id = $this->usuarios->ObtenerId($u->getLogin());
            $_SESSION['id'] = $id;
        }  
        header('Content-Type: application/json');

        $respuesta = array("registro"=>$resp);

        echo json_encode($respuesta,JSON_FORCE_OBJECT);

        
    }

    public function Ingresar(){
        $login = $_POST['login'];
        $password = $_POST['password'];
        

        $usuario = $this->usuarios->Ingresar($login, $password);
        $resp = "error";

        if(isset($usuario->login)){
            $resp = "ok";

            $_SESSION['login'] = $usuario->login;
            $_SESSION['nombre'] = $usuario->nombre; 
            $_SESSION['id'] = $usuario->id;
        }

        header("Content-Type: application/json");

        $respuesta = array("login" => $resp);

        echo json_encode($respuesta, JSON_FORCE_OBJECT);

        // require_once "View/usuarios/header.php";
        // require_once "View/inicio/principal.php";
        // require_once "View/footer.php";
    }

    public function Inicio(){
        //$BD = BaseDeDatos::Conectar();
    
        require_once "View/usuarios/header.php";
        //Que cargue el encabezado de la pagina de la vista header.php
        //require_once "View/Inicio/principal.php";
        //echo "Este es el controlador de Inicio";
        require_once "View/footer.php";
        //Que cargue el footer
    }
}


?>