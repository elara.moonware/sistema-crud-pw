<?php 
    require_once "model/producto.php";

    class ProductoControlador
    {
        private $modelo;

        public function __CONSTRUCT(){
            $this->modelo = new Producto();
        }

        public function Inicio(){
            $BD = BasedeDatos::Conectar();
            require_once "view/header.php";
            require_once "view/producto/index.php";
            require_once "view/footer.php";
        }

        public function FormAgregar(){
            //$BD = BasedeDatos::Conectar();
            $idUsuario = $_SESSION['id'];
            $titulo = "Agregar";
            //si se pasa un id modificar producto, sino agregar un producto
            $p = new Producto();
            if(isset($_GET['id'])){
                $p = $this->modelo->ObtenerId($_GET['id'],$idUsuario);
                $titulo = "Modificar";
            }
            require_once "view/header.php";
            require_once "view/producto/agregar.php";
            require_once "view/footer.php";
        }

        public function Guardar(){
            $idUsuario = $_SESSION['id'];

            $p = new Producto();
            $p->setNombre($_POST['nombreProd']);
            $p->setMarca($_POST['marcaProd']);
            $p->setCosto($_POST['costoProd']);
            $p->setPrecio($_POST['precioProd']);
            $p->setCantidad($_POST['cantidadProd']);
            $p->setId($_POST['idProducto']);
            
            $p->getId() > 0?
            $this->modelo->Actualizar($p, $idUsuario):
            $this->modelo->Insertar($p, $idUsuario);
    
            header("location:?c=producto");
        }

        public function Eliminar(){
            $idUsuario = $_SESSION['id'];

            $this->modelo->Eliminar($_GET['id'], $idUsuario);
            header('location:?c=producto');
        }

        public function Salir(){
            session_destroy();
            header("location:?c=usuarios");
        }

    }
    
?>