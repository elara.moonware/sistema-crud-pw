<?php
class BaseDeDatos
{
    const servidor = "localhost";
    const usuario = "root";
    const password = "";
    const basedatos = "sistema";

    public static function Conectar(){
        try {
            $conexion = new PDO("mysql:host=".self::servidor.";dbname="
            .self::basedatos.";charset=utf8",self::usuario,self::password);
            $conexion->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

            return $conexion;
        } catch (PDOException $e) {
            return "Fallo la conexion a la base de datos".$e->getMessage();
        }
    }
}
?>