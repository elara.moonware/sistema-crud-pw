<?php
class Producto
{
    private $pdo;

    private $id;
    private $nombre;
    private $marca;
    private $costo;
    private $precio;
    private $cantidad;

    public function __CONSTRUCT(){
        $this->pdo = BaseDeDatos::Conectar();
    }

    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
    }

    public function getMarca(){
        return $this->marca;
    }

    public function setMarca($marca){
        $this->marca = $marca;
    }

    public function getCosto(){
        return $this->costo;
    }

    public function setCosto($costo){
        $this->costo = $costo;
    }

    public function getPrecio(){
        return $this->precio;
    }

    public function setPrecio($precio){
        $this->precio = $precio;
    }

    public function getCantidad(){
        return $this->cantidad;
    }

    public function setCantidad($cantidad){
        $this->cantidad = $cantidad;
    }

    //Metodos para acceder a la base de datos

    //metodo cantidad regresa el numero de productos en la bd
    public function Cantidad(){
        try {
            $sql = "SELECT count(cantidad) as Cantidad FROM productos";
            $consulta = $this->pdo->prepare($sql);
            $consulta->execute();

            return $consulta->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }//fin del metodo cantidad

    //Metodo que consulta todos los productos de la base de datos
    public function Listar($idUsuario){
        try {
            $sql = "SELECT * FROM productos where idUsuario = ?";
            $consulta = $this->pdo->prepare($sql);
            $consulta->execute(array($idUsuario));

            return $consulta->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    

    }
    //inserta un objeto $p en la base de datos
    public function Insertar($p,$idUsuario){
        try {
            $sql = "insert into productos(nombre,marca,costo,precio,cantidad,idUsuario)
            values(?,?,?,?,?,?)";

            $this->pdo->prepare($sql)
            ->execute(array(
                $p->getNombre(),
                $p->getMarca(),
                $p->getCosto(),
                $p->getPrecio(),
                $p->getCantidad(),
                $idUsuario
            ));
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function Actualizar($p, $idUsuario){
        try {
            $sql = "update productos set nombre = ?,
            marca = ?, costo=?, precio=?, cantidad=?
            where id = ? and idUsuario = ?";

            $this->pdo->prepare($sql)
            ->execute(array(
                $p->getNombre(),
                $p->getMarca(),
                $p->getCosto(),
                $p->getPrecio(),
                $p->getCantidad(),
                $p->getId(),
                $idUsuario
            ));
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    //metodo obtener id, obtiene la informacion de un producto por id
    public function ObtenerId($id, $idUsuario){
        try {
           $sql = "SELECT * from productos where id = ? and idUsuario=?";
           $consulta = $this->pdo->prepare($sql);
           $consulta->execute(array($id,$idUsuario));
           $res = $consulta->fetch(PDO::FETCH_OBJ);
           $p = new Producto();
           $p->setId($res->id);
           $p->setNombre($res->nombre);
           $p->setMarca($res->marca);
           $p->setCosto($res->costo);
           $p->setPrecio($res->precio);
           $p->setCantidad($res->cantidad);


           return $p;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Eliminar($id){
        try {
            $sql = "DELETE from productos where id = ?";
            $this->pdo->prepare($sql)
                 ->execute(array($id));
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}//fin de la clase producto

?>