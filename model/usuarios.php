<?php
class Usuarios
{
    private $pdo;
    private $id;
    private $login;
    private $password;
    private $nombre;

    public function __CONSTRUCT(){
        $this->pdo = BaseDeDatos::Conectar();
    }

    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getLogin(){
        return $this->login;
    }

    public function setLogin($login){
        $this->login = $login;
    }

    public function getPassword(){
        return $this->password;
    }

    public function setPassword($password){
        $this->password = $password;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
    }

    public function Insertar(Usuarios $u){
        try {
            $sql = "insert into usuarios
            (login,password,nombre) values(?,?,?)";
            $this->pdo->prepare($sql)
            ->execute(array(
                $u->getLogin(),
                $u->getPassword(),
                $u->getNombre()
            ));
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function ExisteUsuario($login){
        try {
            $sql = "select login from usuarios WHERE login = ?";
            $consulta = $this->pdo->prepare($sql);
            $consulta->execute(array($login));

            if($res = $consulta->fetch(PDO::FETCH_OBJ))
                return true;
            else
                return false;
            
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function Ingresar($login, $password){
        try {
            $sql = "select * FROM usuarios WHERE login = ? and password = ?";

            $consulta = $this->pdo->prepare($sql);
            $consulta->execute(array($login,$password));

            return $res = $consulta->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function ObtenerId($login){
        try {
            $sql = "SELECT id from usuarios where login = ?";
            $consulta = $this->pdo->prepare($sql);
            $consulta->execute(array($login));

            if($res = $consulta->fetch(PDO::FETCH_OBJ)){
                return $res->id;
            }else{
                return false;
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}

?>